## First iteration
The first iteration aims to get acquainted with objects, encapsulation, and work with primitive types.

**Keywords:** Class, object, method, attribute, constructor, default constructor, getter, setter, primitive type, reference type, object state, method chaining, javadoc.

1. Create a `cz.muni.fi.pb162.project.demo` **package** and move the class `Main` into it.
2. In the in the `cz.muni.fi.pb162.project` package, create a `Player` class that represents a player of a desktop game. Every player has a `name`. Add a _getter_ and _setter_ for the corresponding text attribute. Do not create a constructor. We will use **the default one** in this iteration.
3. In the in the `cz.muni.fi.pb162.project` package, create a `Coordinates` class representing coordinates on the board. The classic chessboard uses coordinates from A to H and 1 to 8. Therefore, define attributes `letterNumber` and `number`. However, both of them will be of numeric types - primitive integers. We will later easily convert numbers to letters and vice versa using [ASCII](https://en.wikipedia.org/wiki/ASCII)) codes.
    - Add getters and setters that initiates values of both attributes.
    - Add a constructor that takes initial values of the `letterNumber` and `number` attributes (in this order) and initiates them during the object's instantiation.
    - Add the `averageOfCoordinates` method, which returns the average number of `letterNumber` and `number`. Choose the appropriate return type.
    - Add the `add` method, which takes other coordinates as an input parameter and calculates the new coordinates by summing the two. You can calculate the sum by summing the individual components (just as complex numbers). The implementation does not change the _state_ of the original object and enables _method chaining_, e.g.,
       ```
             Coordinates c = (new Coordinates(1, 2))
                                    .add(new Coordinate(3, 5))
                                    .add(new Coordinate(7, 0));
      ```
5. In the in the `cz.muni.fi.pb162.project` package,, create a `Piece` class that represents a piece of a board game, e.g., a king or knight in chess. Every piece has an attribute `id` of type "64-bit integer".
    - Add a constructor and a getter. Omit setter, as we do not need to change the object's state.
6. Edit the executable class `Main`.
    - Remove the print statement for `Hello World`.
    - Create a new player. Set the name to "Matko" and print the name to standard output using the player's methods.
    - Create two sets of coordinates. The first is `(1, 7)`, the second is `(5, 0)`.
    - Print the average of the first set of coordinates.
    - Compute the sum of the two sets of coordinates and print the resulting two components on two separate lines.
7. Every submitted iteration **must pass tests and checkstyle**.
   - Test your code using unit tests in the `src/test/java` package.
   - To run all tests and checkstyle, run `mvn clean test` from the command line.

**Hints:**
- Do not forget to write the documentation.
   - Every class must contain `@author <YourName>`, e.g., `@author Jan Hrasko`.
   - Every public method must include documentation, except for getters, setters, and overridden methods (explained later).
        - If the method has an input parameter, `@param <nameOfParameter>` with a description must be in the documentation.
        - If the method has a return value, `@returns` with a description must be in the documentation.
   - If you type `/**` above the method and press enter, the IDE will automatically generate tags that you must fill in to create proper documentation of the method.
- Standard `int` type has 32-bit precision. 
